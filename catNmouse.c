///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Jordan Cortado <jcortado@hawaii.edu>
/// @date    25_Jan_2022
///////////////////////////////////////////////////////////////////////////////

#define DEFAULT_MAX_NUMBER (2048)

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char* argv[] ) {

   int theMaxValue = DEFAULT_MAX_NUMBER;

   srand(time(NULL));


   if(argc >=  2){
      int possibleMax = atoi( argv[1] );
      if(possibleMax < 1){
         printf("The argument in the command line must be greater than one.\n");
         exit( 1 );
      }
      theMaxValue = possibleMax;
   }
   
   int mouse_num = ( rand() % theMaxValue ) + 1; 
   //Generate a random number from 1 to default max of 2048
                                      
   int cat_guess;

   do{

      printf("Ok cat, I'm thinking of a number from 1 to %d. Make a guess: ", theMaxValue );

      // User input  
      scanf( "%d", &cat_guess );

      if (cat_guess < 1){
         printf("You must enter a number that's >= 1\n");
      }
      else if (cat_guess > theMaxValue){
         printf("You must enter a number <= %d\n", theMaxValue );
      }
      else if (cat_guess > mouse_num){
         printf("No cat... the number I'm thinking of is lower than than %d\n", cat_guess);
      }
      else if (cat_guess < mouse_num){
         printf("No cat... the number I'm thinking of is higher than %d\n", cat_guess);
      }
   }while (cat_guess != mouse_num);

   if (cat_guess == mouse_num){
      printf("You got me.\n");
      printf(" \\    /\\ \n");
      printf("  )  ( ') \n");
      printf(" (  /  ) \n"); 
      printf("  \\\(__)| \n");

      exit( 0 );
   }


}

